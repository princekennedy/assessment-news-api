### Assessment news API


## Getting started

To 
make 
it easy for you to get started with NEWS API , here's a list of steps.

## Install with composer
Clone the project
```
 $ git clone https://gitlab.com/princekennedy/assessment-news-api.git
 $ composer install

```

## Set Enviroment

```
 $ copy .env.example .env

```

## Set the application key

```
 $ php artisan key:generate

```


## Run the system

```
 $ php artisan serve

```
It will run on localhost at port 8000 to change port and IP address run 
```
 $ php artisan serve --host="Your IP" --port="Your port"

```

## Getting with Curl

Articles
```
 $ curl -H 'content-type: application/json' -H 'Accept: application/json' -v -X GET http://127.0.0.1:8000/api/articles
```
Top Articles
```
 $ curl -H 'content-type: application/json' -H 'Accept: application/json' -v -X GET http://127.0.0.1:8000/api/top-articles
```

NOTE: Data is Cached for 10minutes in order to increase data cache time change in .env file and you cal also change cache driver default is file (file, database, redis)

```
 BROADCAST_DRIVER=log
 CACHE_DRIVER=file
 FILESYSTEM_DRIVER=local
 QUEUE_CONNECTION=sync
 SESSION_DRIVER=file
 SESSION_LIFETIME=120
 MEMCACHED_HOST=127.0.0.1
 DATA_CACHE_TIME=10

```


## Query Parameters

Parameter, Default and Description

## q	
	
Keep articles that matched keywords.
```
 Default: None
 Example: http://127.0.0.1:8000/api/articles?filters[q]=None
```

## topic	
	
Set the articles topic. Topics available are breaking-news, world, nation, business, technology, entertainment, sports, science and health.
```
 Default: breaking-news
 Example: http://127.0.0.1:8000/api/articles?filters[topic]=breaking-news
```

## lang	
	
Set the language of returned articles (list of supported languages).
country	Any	Set the country of returned articles (list of supported countries).

```
 Default: Any
 Example: http://127.0.0.1:8000/api/articles?filters[lang]=Any
```

## max	

Set the maximum number of articles returned per request. 100 is the maximum value. The maximum value allowed depend on your plan (see pricing for more information).
```
 Default: 10
 Example: http://127.0.0.1:8000/api/articles?filters[max]=10
```

## from	

Keep articles with a publication date greater than or equal to the given date. ISO 8601 format (e.g. 2022-10-24T14:51:04Z)
```
 Default: None
 Example: http://127.0.0.1:8000/api/articles?filters[from]=None
```

## to	

Keep articles with a publication date less than or equal to the given date. ISO 8601 format (e.g. 2022-10-24T14:51:04Z)
```
 Default: None
 Example: http://127.0.0.1:8000/api/articles?filters[to]=None
```

## Multiple filters	

Keep articles with a publication date less than or equal to the given date. ISO 8601 format (e.g. 2022-10-24T14:51:04Z)
```
 Default: None
 Example: http://127.0.0.1:8000/api/articles?filters[max]=20&filters[topic]=technology
```

## Authors and acknowledgment
Appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
In Development