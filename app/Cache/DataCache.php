<?php   
namespace App\Cache;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use Cache;

/**
 * 
 */
class DataCache
{

	public function getCacheTime(){

		return Carbon::now()->addMinutes( env("DATA_CACHE_TIME") ); // Return cache time in minutes

	}

    public function getUrl($endpoint){

    	return env("GNEWS_API_URL") . "/" . $endpoint . "?";

    }

    public function getAddBody($url, $params = []){

    	$params["token"] = env("GNEWS_API_KEY");

    	$filters = ( request()->filters ) ? request()->filters : [] ;

    	$params = array_merge($params, $filters);

    	foreach($params as $key => $value){

    		$url .= $key . "=" . $value . "&";

    	}

    	return $url;

    }

    /**
     * @param \Illuminate\Http\Request $request, Data limit
     * @return \Illuminate\Http\Response
     */
	public function getArticles(Request $request){

		$url = $this->getUrl("search");

		$body = ["q" => $request->search ?? "example"];

		$url = $this->getAddBody($url, $body);

		// Cache data by url
		return Cache::remember($url, $this->getCacheTime() , function() use($url){

			$response = Http::get($url);

			if($response->ok()){

	            return $response->json();

	        } else {

	        	return null;

	        }

		});

	}

    /**
     * @param \Illuminate\Http\Request $request, Data limit
     * @return \Illuminate\Http\Response
     */
	public function getTopArticles(Request $request){

		$url = $this->getUrl("top-headlines");

		$body = [];

		$url = $this->getAddBody($url, $body);

		// Cache data by url
		return Cache::remember($url, $this->getCacheTime() , function() use($url){

			$response = Http::get($url);

			if($response->ok()){

	            return $response->json();

	        } else {

	        	return null;

	        }

		});

	}



}