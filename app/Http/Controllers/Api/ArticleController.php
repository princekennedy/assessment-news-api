<?php

namespace App\Http\Controllers\Api;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleResource;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\ArticleCollection;
use App\Http\Requests\ArticleStoreRequest;
use App\Http\Requests\ArticleUpdateRequest;

use Facades\App\Cache\DataCache;

class ArticleController extends Controller
{


    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function articles(Request $request)
    {

        // $this->authorize('view-any', Article::class);

        $articles = DataCache::getArticles($request);

        return response($articles);

    }


    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function topArticles(Request $request)
    {

        // $this->authorize('view-any', Article::class);

        $articles = DataCache::getTopArticles($request);

        return response($articles);

    }

}
