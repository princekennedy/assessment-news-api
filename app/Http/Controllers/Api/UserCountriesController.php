<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CountryResource;
use App\Http\Resources\CountryCollection;

class UserCountriesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        $this->authorize('view', $user);

        $search = $request->get('search', '');

        $countries = $user
            ->countries2()
            ->search($search)
            ->latest()
            ->paginate();

        return new CountryCollection($countries);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $this->authorize('create', Country::class);

        $validated = $request->validate([
            'name' => ['nullable', 'max:255', 'string'],
            'shortname' => ['nullable', 'max:255', 'string'],
        ]);

        $country = $user->countries2()->create($validated);

        return new CountryResource($country);
    }
}
