<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use App\Models\Scopes\Searchable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;
    use HasFactory;
    use Searchable;
    use SoftDeletes;
    use HasApiTokens;

    protected $fillable = ['name', 'email', 'phone', 'password'];

    protected $searchableFields = ['*'];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function countries()
    {
        return $this->hasMany(Country::class, 'created_by');
    }

    public function countries2()
    {
        return $this->hasMany(Country::class, 'updated_by');
    }

    public function articles()
    {
        return $this->hasMany(Article::class, 'created_by');
    }

    public function articles()
    {
        return $this->hasMany(Article::class, 'updated_by');
    }

    public function isSuperAdmin()
    {
        return $this->hasRole('super-admin');
    }
}
