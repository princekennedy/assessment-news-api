@php $editing = isset($country) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="name"
            label="Name"
            :value="old('name', ($editing ? $country->name : ''))"
            maxlength="255"
            placeholder="Name"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="shortname"
            label="Shortname"
            :value="old('shortname', ($editing ? $country->shortname : ''))"
            maxlength="255"
            placeholder="Shortname"
        ></x-inputs.text>
    </x-inputs.group>
</div>
